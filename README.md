# QOwnNote Scripts

This repository contain some QOwnNote scripts I wrote for my personal usage

## meetingnotes

Template to create meeting notes. The script will automatically create a note `meeting YYYY-MM-DD` which contains the typical sections I use for my notes

## projectfolder

Initialize a project folder with an "Overview.md" and a "Next Steps.md" file

## workreports

Template to create a new workreport for the current week or jump to it if it already exists
