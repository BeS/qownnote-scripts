import QtQml 2.0
import com.qownnotes.noteapi 1.0

/**
 * This script creates the initial files for a projects folder
 */
QtObject {
    property string scriptDirPath;
    property bool debug: false;

    /**
     * Initializes the custom action
     */
    function init() {
        if (debug === true) script.log("Debug mode enabled");
        script.registerCustomAction("projectFolder", "Create initial files for project folders", "Init Project Folder", scriptDirPath + "/project.png");
    }

    /**
     * This function is invoked when a custom action is triggered
     * in the menu or via button
     *
     * @param identifier string the identifier defined in registerCustomAction
     */
    function customActionInvoked(identifier) {
        if (identifier != "projectFolder") {
            return;
        }

        var overviewFileName = "Overview.md";
        var file = script.fetchNoteByFileName(overviewFileName);

        // check if file was found
        if (file.id == 0) {
            if (debug === true) script.log("creating new Overview file");
            // Create the new overview file.
            script.createNote("Overview\n" + "========\n\nAcount Manager: \n\nContact: \n\nAndrede: \n\n## Summary\n\n## References\n\n### Professional Service Tickets\n\n## Progress\n\n\n");
        } else {
            if (debug === true) script.log("Overview file already exists, ID: " + file.id);
        }

        var nextStepsFileName = "Next Steps.md";
        file = script.fetchNoteByFileName(nextStepsFileName);

        // check if file was found
        if (file.id == 0) {
            if (debug === true) script.log("creating new Next Steps file");
            script.createNote("Next Steps\n" + "==========\n\n\n");
        } else {
            if (debug === true) script.log("Next Steps file already exists, ID: " + file.id);
        }
    }
}
