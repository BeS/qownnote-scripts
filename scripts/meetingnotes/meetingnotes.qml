import QtQml 2.0
import com.qownnotes.noteapi 1.0

/**
 * This script creates a new meeting note in the current folder
 */
QtObject {
    property string headlinePrefix;
    property string defaultTags;
    property bool timeInNoteName;
    property string scriptDirPath;
    property bool debug: false;

    // register your settings variables so the user can set them in the script settings
    property variant settingsVariables: [
        {
            "identifier": "headlinePrefix",
            "name": "Headline prefix",
            "description": "Please enter a prefix for your meeting notes headline:",
            "type": "string",
            "default": "Meeting",
        },
        {
            "identifier": "defaultTags",
            "name": "Default tags",
            "description": "One or more default tags (separated by commas) to assign to a newly created meeting notes. Leave blank to disable auto-tagging.",
            "type": "string",
            "default": "meeting",
        },
        {
            "identifier": "timeInNoteName",
            "name": "Time in meeting note",
            "description": "Add time (HH:mm) in 'Workreport' note name.",
            "type": "boolean",
            "default": false,
        },
    ];

    /**
     * Initializes the custom action
     */
    function init() {
	if (debug === true) script.log("Debug mode enabled");
        script.registerCustomAction("meetingNote", "Create a new meeting note", "Meeting Note", scriptDirPath + "/meeting.png");
    }

    /**
     * This function is invoked when a custom action is triggered
     * in the menu or via button
     *
     * @param identifier string the identifier defined in registerCustomAction
     */
    function customActionInvoked(identifier) {
        if (identifier != "meetingNote") {
            return;
        }

        // get the date headline
        var m = new Date();
        var currentDate = m.getFullYear() + "-" +("0" + (m.getMonth()+1)).slice(-2) + "-" + ("0" + m.getDate()).slice(-2);
        var headline = headlinePrefix + " " + currentDate;

        if (timeInNoteName) {
          headline = headline + " " + ("0" + m.getHours()).slice(-2) + "." + ("0" + m.getMinutes()).slice(-2);
        }

        var fileName = headline + ".md";

        // Check if we already have a meeting note for today.
	if(debug ===true ) script.log(fileName);
        var note = script.fetchNoteByFileName(fileName);
	if (debug=== true) script.log(note);

        // check if note was found
        if (note.id > 0) {
            // jump to the note if it was found
            if (debug === true) script.log("found meeting note: " + headline);
            script.setCurrentNote(note);
        } else {
            // create a new meeting note if it wasn't found
            // keep in mind that the note will not be created instantly on the disk
            if (debug === true) script.log("creating new meeting note: " + headline);

            // Create the new meeting note.
            script.createNote(headline + "\n" + "=".repeat(headline.length)  + "\n\n## Meeting Details\n\n#### Meeting Date\n\n" + currentDate + "\n\n#### Meeting Type\n\n* [ ] phone\n* [ ] on-side\n\n#### Location\n\n\n\n#### Participants\n\n\n\n#### Topic\n\n\n\n## Preparation\n\n\n\n## Notes\n\n\n\n## Action Items\n\n\n\n");

            // Default tags.
            if (defaultTags && defaultTags !== '') {
                defaultTags
                    // Split on 0..* ws, 1..* commas, 0..* ws.
                    .split(/\s*,+\s*/)
                    .forEach(function(i) {
                        if (debug === true) script.log('Tag the new meeting note with default tag: ' + i);
                        script.tagCurrentNote(i);
                    });
            }
        }
    }
}
